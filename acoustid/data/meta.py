# Copyright (C) 2011 Lukas Lalinsky
# Distributed under the MIT license, see the LICENSE file for details.

import logging
from acoustid import models
from sqlalchemy import sql
from acoustid import tables as schema
from acoustid.data.track import lookup_meta_ids
from acoustid.data.rating import delete_ratings
from sqlalchemy.orm import sessionmaker
from time import sleep

logger = logging.getLogger(__name__)


def insert_meta(conn, values):
    insert_stmt = schema.meta.insert().values(**values)
    id = conn.execute(insert_stmt).inserted_primary_key[0]
    logger.debug("Inserted meta %d with values %r", id, values)
    return id

def delete_track_meta_source(conn, meta_source_id):
    query = schema.track_meta_source.delete(schema.track_meta_source.c.track_meta_id == meta_source_id)
    conn.execute(query)

def get_track_meta(conn, meta_id):
    query = sql.select([schema.track_meta.c.id], schema.track_meta.c.meta_id == meta_id)
    return conn.execute(query).scalar()

def delete_track_meta(conn, meta_id):
    query = schema.track_meta.delete(schema.track_meta.c.meta_id == meta_id)
    conn.execute(query)

def delete_meta(conn, meta_id):
    query = schema.meta.delete(schema.meta.c.id == meta_id)
    conn.execute(query)

def delete_metas(conn, meta_ids):
    query = schema.meta.delete(schema.meta.c.id.in_(meta_ids))
    conn.execute(query)

def delete_submission(conn, meta_id):
    query = schema.submission.delete(schema.submission.c.meta_id == meta_id)
    conn.execute(query)

def check_can_merge_meta(conn, track_id):
    meta_ids = lookup_meta_ids(conn, [ track_id ])
    if not meta_ids:
        return
    meta_ids = meta_ids[track_id]
    meta = lookup_meta(conn, meta_ids)
    if len(meta) < 2:
        return
    if meta[0]['points'] > 5 and meta[1]['points'] < 4:
        del meta[1]
    elif len(meta) > 4:
        del meta[0]
        del meta[0]
        del meta[0]
    else:
        return
    meta_ids = [d['release_id'] for d in meta]
    delete_metas(conn, meta_ids)

def lookup_meta_by_text(conn, text):
    Session = sessionmaker()
    session = Session(bind=conn)
    _search_vector = sql.func.plainto_tsquery(sql.text("'english'"), text)
    score = sql.func.ts_rank_cd(schema.meta.c.query, _search_vector, 32).label('score')
    query = session.query(schema.meta, score).filter(schema.meta.c.query.op('@@')(_search_vector),).order_by(score.desc())

    result_tmp = query.first()
    if not result_tmp:
        return None
    result = {
        'id': result_tmp.id,
        'track': result_tmp.track,
        'artists': [ result_tmp.artist ],
        'album': result_tmp.album,
        'group_artists': [ result_tmp.album_artist ],
        'track_position': result_tmp.track_no,
        'disc': result_tmp.disc_no,
        'year': result_tmp.year,
        'genre': result_tmp.genre,
        'cover': result_tmp.cover,
        'track_count': result_tmp.track_count,
        'score': result_tmp.score
    }
    session.close()
    return result


def lookup_meta(conn, meta_ids):
    if not meta_ids:
        return []
    query = schema.meta.select(schema.meta.c.id.in_(meta_ids)).order_by(schema.meta.c.points.desc())
    results = []
    for row in conn.execute(query):
        result = {
            '_no_ids': True,
            'recording_id': row['id'],
            'recording_title': row['track'],
            'recording_artists': [],
            'recording_duration': None,
            'track_id': row['id'],
            'track_position': row['track_no'],
            'track_title': row['track'],
            'track_artists': [],
            'track_duration': None,
            'medium_position': row['disc_no'],
            'medium_format': None,
            'medium_title': None,
            'medium_track_count': None,
            'release_rid': row['id'],
            'release_id': row['id'],
            'release_title': row['album'],
            'release_artists': [],
            'release_medium_count': None,
            'release_track_count': row['track_count'],
            'release_genre': row['genre'],
            'release_position': row['track_no'],
            'release_cover': row['cover'],
            'release_disc': row['disc_no'],
            'release_disc_count': row['disc_count'],
            'release_provider': row['provider'],
            'release_year': row['year'],
            'release_events': [{
                'release_date_year': row['year'],
                'release_date_month': None,
                'release_date_day': None,
                'release_country': '',
            }],
            'release_group_id': row['id'],
            'release_group_title': row['album'],
            'release_group_artists': [],
            'release_group_primary_type': None,
            'release_group_secondary_types': [],
            'points': row['points'],
        }
        if row['artist']:
            result['recording_artists'].append(row['artist'])
            result['track_artists'].append(row['artist'])
        if row['album_artist']:
            result['release_artists'].append(row['album_artist'])
            result['release_group_artists'].append(row['album_artist'])
        results.append(result)
    return results

def lookup_meta_2(conn, meta_ids):
    if not meta_ids:
        return []
    query = schema.meta.select(schema.meta.c.id.in_(meta_ids))
    results = []
    for row in conn.execute(query):
        result = {
            'track_id': row['id'],
            'track_position': row['track_no'],
            'track_title': row['track'],
            'track_artists': []
        }
        if row['artist']:
            result['track_artists'].append(row['artist'])
        results.append(result)
    return results
