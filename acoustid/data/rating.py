import logging
from acoustid import models
from sqlalchemy import sql, func
from acoustid import tables as schema

logger = logging.getLogger(__name__)

def delete_ratings(conn, meta_id):
    query = schema.meta_rating.delete(schema.meta_rating.c.meta_id == meta_id)
    conn.execute(query)

def insert_rating(conn, values):
    query = sql.select([schema.track_meta.c.track_id]).\
        where(schema.track_meta.c.meta_id == values.get('meta_id'))

    track_id = conn.execute(query).first()
    values['track_id'] = track_id[0]

    query = sql.select([schema.meta_rating]).\
        where(sql.and_(schema.meta_rating.c.track_id == values.get('track_id'), schema.meta_rating.c.meta_id == values.get('meta_id'), schema.meta_rating.c.ip_address == values.get('ip_address')))
    if conn.execute(query).scalar() is None:
        insert_stmt = schema.meta_rating.insert().values(**values)
        count = conn.execute(insert_stmt)
    else:
        update_stmt = schema.meta_rating.update().\
            where(sql.and_(schema.meta_rating.c.track_id == values.get('track_id'), schema.meta_rating.c.meta_id == values.get('meta_id'), schema.meta_rating.c.ip_address == values.get('ip_address'))).\
            values(correct=values.get('correct'))
        count = conn.execute(update_stmt)
    query0 = sql.select([func.count()]).\
        where(sql.and_(schema.meta_rating.c.track_id == values.get('track_id'), schema.meta_rating.c.meta_id == values.get('meta_id'), schema.meta_rating.c.correct == True))
    count0 = conn.execute(query0).scalar()
    query1 = sql.select([func.count()]).\
        where(sql.and_(schema.meta_rating.c.track_id == values.get('track_id'), schema.meta_rating.c.meta_id == values.get('meta_id'), schema.meta_rating.c.correct == False))
    count1 = conn.execute(query1).scalar()
    update_stmt0 = schema.meta.update().\
        where(schema.meta.c.id == values.get('meta_id')).\
        values(points=(count0 - count1))
    conn.execute(update_stmt0)
    return 1
