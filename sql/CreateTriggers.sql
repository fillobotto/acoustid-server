CREATE TRIGGER meta_query_update_trigger
BEFORE INSERT OR UPDATE
ON meta FOR EACH ROW EXECUTE PROCEDURE
tsvector_update_trigger(query, 'pg_catalog.english', track, artist, album);